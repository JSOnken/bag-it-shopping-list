//
//  ListRowController.swift
//  Bag It! Shopping List WatchKit Extension
//
//  Created by Jason Onken on 4/14/18.
//  Copyright © 2018 JasonOnken. All rights reserved.
//

import WatchKit

class ListRowController: NSObject {

    @IBOutlet var titleLabel: WKInterfaceLabel!
}
