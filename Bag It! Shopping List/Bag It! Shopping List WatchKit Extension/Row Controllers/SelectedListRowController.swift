//
//  SelectedListRowController.swift
//  Bag It! Shopping List WatchKit Extension
//
//  Created by Jason Onken on 4/14/18.
//  Copyright © 2018 JasonOnken. All rights reserved.
//

import WatchKit

class SelectedListRowController: NSObject {

    @IBOutlet var titleLabel: WKInterfaceLabel!
    @IBOutlet var countLabel: WKInterfaceLabel!
}
