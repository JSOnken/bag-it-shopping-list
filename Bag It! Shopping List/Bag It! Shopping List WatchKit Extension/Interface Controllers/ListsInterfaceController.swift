//
//  InterfaceController.swift
//  Bag It! Shopping List WatchKit Extension
//
//  Created by Jason Onken on 4/12/18.
//  Copyright © 2018 JasonOnken. All rights reserved.
//

import WatchKit
import Foundation


class ListsInterfaceController: WKInterfaceController {

    let lists = ["Groceries", "BBQ", "Birthday Party", "Drug Store", "Weekend Dinner"]
    
    @IBOutlet var listsTable: WKInterfaceTable!
    
    func tableRefresh(){
        listsTable.setNumberOfRows(lists.count, withRowType: "ListsRow")
        for index in 0..<lists.count{
            let row = listsTable.rowController(at: index) as! ListRowController
            row.titleLabel.setText(lists[index])
        }
    }
    
    override func table(_ table: WKInterfaceTable, didSelectRowAt rowIndex: Int) {
        let context = lists[rowIndex]
        presentController(withName: "Selected", context: context)
    }
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
        tableRefresh()
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

}
