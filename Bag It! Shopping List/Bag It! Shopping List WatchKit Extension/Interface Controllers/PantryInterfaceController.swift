//
//  PantryInterfaceController.swift
//  Bag It! Shopping List WatchKit Extension
//
//  Created by Jason Onken on 4/14/18.
//  Copyright © 2018 JasonOnken. All rights reserved.
//

import Foundation
import WatchKit

class PantryInterfaceController: WKInterfaceController, RemoveItemDelegate {
    
    
    var items = ["Chicken Breast", "Pork Chops", "Meatballs", "Milk", "Cheese"]
    var amount = ["Low", "Low", "Full", "Low", "Full"]
    
    @IBOutlet var pantryTable: WKInterfaceTable!
    
    func tableRefresh(){
        pantryTable.setNumberOfRows(items.count, withRowType: "PantryRow")
        for index in 0..<items.count{
            let row = pantryTable.rowController(at: index) as! PantryRowController
            row.titleLabel.setText(items[index])
            row.amountLabel.setText(amount[index])
            let amountString = amount[index]
            if(amountString == "Low") {
                row.amountLabel.setTextColor(UIColor.red)
            } else{
                row.amountLabel.setTextColor(UIColor.green)
            }
        }
    }
    
    override func table(_ table: WKInterfaceTable, didSelectRowAt rowIndex: Int) {
        let context = rowIndex
        presentController(withName: "Amount", context: context)
    }
    
    func removeItemClicked(index: Int) {
        items.remove(at: index)
        amount.remove(at: index)
        tableRefresh()
    }
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
        tableRefresh()
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
}
