//
//  CreateInterfaceController.swift
//  Bag It! Shopping List WatchKit Extension
//
//  Created by Jason Onken on 4/14/18.
//  Copyright © 2018 JasonOnken. All rights reserved.
//

import Foundation
import WatchKit

class CreateInterfaceController: WKInterfaceController {

    let listTypes = ["Groceries", "Drug Store", "Party", "To-Do"]
    
    @IBOutlet var createTable: WKInterfaceTable!
    
    func tableRefresh(){
        createTable.setNumberOfRows(listTypes.count, withRowType: "CreateRow")
        for index in 0..<listTypes.count{
            let row = createTable.rowController(at: index) as! CreateRowController
            row.litsTypeLable.setText(listTypes[index])
        }
    }
    
    override func table(_ table: WKInterfaceTable, didSelectRowAt rowIndex: Int) {
        let context = listTypes[rowIndex]
        presentController(withName: "Add", context: context)
    }
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
        tableRefresh()
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
}
