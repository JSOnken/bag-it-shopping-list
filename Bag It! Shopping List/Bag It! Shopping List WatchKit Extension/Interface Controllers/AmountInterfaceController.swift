//
//  AmountInterfaceController.swift
//  Bag It! Shopping List WatchKit Extension
//
//  Created by Jason Onken on 4/14/18.
//  Copyright © 2018 JasonOnken. All rights reserved.
//

import Foundation
import WatchKit

protocol RemoveItemDelegate{
    func removeItemClicked(index: Int)
}

class AmountInterfaceController: WKInterfaceController {

    var full = true
    var selectedIndex = 0
    
    var delegate: PantryInterfaceController?
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        if let context = context{
            selectedIndex = context as! Int
        }
    }
    
    override func willActivate() {
        super.willActivate()
    }
    
    override func didDeactivate() {
        super.didDeactivate()
    }
    
    @IBAction func switchToggled(_ value: Bool) {
        if value == false{
            full = false
        } else{
            full = true
        }
    }
    
    @IBAction func doneClicked() {
        dismiss()
    }
    
    @IBAction func removedClicked() {
        self.delegate?.removeItemClicked(index: selectedIndex)
        dismiss()
    }
}
