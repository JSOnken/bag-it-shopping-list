//
//  SelectedListInterfaceController.swift
//  Bag It! Shopping List WatchKit Extension
//
//  Created by Jason Onken on 4/14/18.
//  Copyright © 2018 JasonOnken. All rights reserved.
//

import Foundation
import WatchKit

class SelectedListInterfaceController: WKInterfaceController {
    
    var contextDictionary = [String:String]()
    let items = ["Chicken Breast", "Pork Chops", "Meatballs", "Milk", "Cheese", "Apples", "Oranges"]
    let amounts = ["5", "8", "1 lbs", "1 gallon", "1 bag", "1 bag", "1 dozen"]
    
    @IBOutlet var selectedListTable: WKInterfaceTable!
    
    func tableRefresh(){
        selectedListTable.setNumberOfRows(items.count, withRowType: "SelectedRow")
        for index in 0..<items.count{
            let row = selectedListTable.rowController(at: index) as! SelectedListRowController
            row.titleLabel.setText(items[index])
            row.countLabel.setText(amounts[index])
        }
    }
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        if let context = context{
            let listName = context as! String
            setTitle("< " + listName)
        }
        // Configure interface objects here.
        tableRefresh()
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
}
