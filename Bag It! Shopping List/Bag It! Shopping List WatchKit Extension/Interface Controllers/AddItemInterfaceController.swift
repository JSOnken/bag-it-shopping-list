//
//  AddItemInterfaceController.swift
//  Bag It! Shopping List WatchKit Extension
//
//  Created by Jason Onken on 4/14/18.
//  Copyright © 2018 JasonOnken. All rights reserved.
//

import Foundation
import WatchKit

class AddItemInterfaceController: WKInterfaceController {

    var itemArray = [String]()
    
    @IBOutlet var itemsTable: WKInterfaceTable!
    @IBOutlet var nameLabel: WKInterfaceLabel!
    
    @IBAction func addItem() {
        let suggestions = ["Milk", "Eggs", "Cheese", "Yogurt", "Cereal", "Chips", "Bread", "Hot Dog Buns",
                           "Hamburger Buns", "Chicken", "Pork Chops", "Steak", "Hot Dogs"]
        
        presentTextInputController(withSuggestions: suggestions, allowedInputMode: .plain) { (results) in
            guard let responses = results else {
                print("No result found")
                return
            }
            let item = responses[0] as! String
            self.itemArray.append(item)
            self.tableRefresh()
        }
    }
    
    @IBAction func doneClicked() {
        dismiss()
    }
    
    func tableRefresh(){
        itemsTable.setNumberOfRows(itemArray.count, withRowType: "AddItemRow")
        for index in 0..<itemArray.count{
            let row = itemsTable.rowController(at: index) as! AddItemRowController
            row.itemLabel.setText(itemArray[index])
        }
    }
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        if let context = context{
            let listName = context as! String
            nameLabel.setText(listName)
        }
    }
}
