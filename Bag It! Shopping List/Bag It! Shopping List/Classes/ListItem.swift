//
//  ListItem.swift
//  Bag It! Shopping List
//
//  Created by Jason Onken on 4/26/18.
//  Copyright © 2018 JasonOnken. All rights reserved.
//

import Foundation

class ListItem: NSCoding {
    
    // Variables
    var name: String
    var count: Int
    var notes: String
    var amount: String?
    
    // List item initializer
    init(name: String, count: Int, notes: String) {
        self.name = name
        self.count = count
        self.notes = notes
    }
    
    // Pantry item initializer
    init(name: String, count: Int, notes: String, amount: String) {
        self.name = name
        self.count = count
        self.notes = notes
        self.amount = amount
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        self.init(name: "Awesome item", count: 999, notes: "Metric tons", amount: "Overflowing")
        
        name = aDecoder.decodeObject(forKey: "nameString") as! String
        count = aDecoder.decodeInteger(forKey: "countInt")
        notes = aDecoder.decodeObject(forKey: "notesString") as! String
        amount = aDecoder.decodeObject(forKey: "amountString") as? String
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(name, forKey: "nameString")
        aCoder.encode(count, forKey: "countInt")
        aCoder.encode(notes, forKey: "notesString")
        aCoder.encode(amount, forKey: "amountString")
    }
    
}
