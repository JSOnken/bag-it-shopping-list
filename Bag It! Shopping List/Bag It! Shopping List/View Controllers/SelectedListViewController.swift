//
//  SelectedListViewController.swift
//  Bag It! Shopping List
//
//  Created by Jason Onken on 4/25/18.
//  Copyright © 2018 JasonOnken. All rights reserved.
//

import UIKit

class SelectedListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var selectedListTable: UITableView!
    
    var selectedList: String?
    var newItem = ""
//    var listItems = ["Milk", "Cheese", "Sour cream", "Apples", "Steak", "Chicken breasts"]
    var itemsList = [String]()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        guard let listName = selectedList else {
            fatalError("No list name found!")
        }
        self.title = listName
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return itemsList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = selectedListTable.dequeueReusableCell(withIdentifier: "selectedListCell", for: indexPath)
        
        cell.textLabel?.text = itemsList[indexPath.row]
        
        return cell
    }
}
