//
//  CustomTabBarController.swift
//  Bag It! Shopping List
//
//  Created by Jason Onken on 4/25/18.
//  Copyright © 2018 JasonOnken. All rights reserved.
//

import Foundation
import UIKit

// Custom tab bar controller class to hold a copy of the memories array for passing between view controllers
class CustomTabBarController: UITabBarController {
    
    var lists = [String]()
}
