//
//  SettingsViewController.swift
//  Bag It! Shopping List
//
//  Created by Jason Onken on 4/16/18.
//  Copyright © 2018 JasonOnken. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController, UITabBarDelegate, UITableViewDataSource {
    
    @IBOutlet weak var settingsTable: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = settingsTable.dequeueReusableCell(withIdentifier: "settingsCell", for: indexPath)
        
        
        return cell
    }
}
