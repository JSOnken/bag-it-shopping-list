//
//  PantryViewController.swift
//  Bag It! Shopping List
//
//  Created by Jason Onken on 4/16/18.
//  Copyright © 2018 JasonOnken. All rights reserved.
//

import UIKit

class PantryViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {
    
    @IBOutlet weak var pantryTable: UITableView!
    @IBOutlet weak var addItemET: UITextField!
    
    var pantryList = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pantryList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = pantryTable.dequeueReusableCell(withIdentifier: "pantryCell", for: indexPath)
        
        cell.textLabel?.text = pantryList[indexPath.row]
        
        return cell
    }
    
    @IBAction func addItemClicked(_ sender: Any) {
        guard let enteredItem = addItemET.text else {
            print("Could not locate text")
            return
        }
        pantryList.append(enteredItem)
        addItemET.text?.removeAll()
        addItemET.resignFirstResponder()
        pantryTable.reloadData()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        addItemET.resignFirstResponder()
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextFieldDidEndEditingReason) {
        guard let enteredItem = addItemET.text else {
            print("Could not locate text")
            return
        }
        pantryList.append(enteredItem)
        addItemET.text?.removeAll()
        pantryTable.reloadData()
    }
}
