//
//  ListsViewController.swift
//  Bag It! Shopping List
//
//  Created by Jason Onken on 4/25/18.
//  Copyright © 2018 JasonOnken. All rights reserved.
//

import UIKit

class ListsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    

    @IBOutlet weak var listsTable: UITableView!
    
    var listDictionary = Dictionary<String, [ListItem]>()
    var createdLists = [String]()
    var newList = ""
    var selectedIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func createClicked(_ sender: Any) {
        let alertController = UIAlertController(title: "New List", message: "Please enter your list name.", preferredStyle: .alert)
        let saveList = UIAlertAction(title: "Save", style: .default) { (_) in
            if let listName = alertController.textFields?[0] {
                self.newList = listName.text!
            } else {
                self.newList = ""
            }
            self.createdLists.append(self.newList)
            self.listDictionary[self.newList] = [ListItem]()
            self.listsTable.reloadData()
        }
        
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        alertController.addTextField(configurationHandler: { (textField) in
            textField.placeholder = "Enter list name"
        })
        
        alertController.addAction(saveList)
        alertController.addAction(cancel)
        
        present(alertController, animated: true, completion: nil)
        
        listsTable.reloadData()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return createdLists.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = listsTable.dequeueReusableCell(withIdentifier: "listsCell", for: indexPath)
        
        cell.textLabel?.text = createdLists[indexPath.row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndex = indexPath.row
        performSegue(withIdentifier: "toSelectedList", sender: self)
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destination = segue.destination as! SelectedListViewController
        destination.selectedList = createdLists[selectedIndex]
    }
    

}
