//
//  AddItemViewController.swift
//  Bag It! Shopping List
//
//  Created by Jason Onken on 4/26/18.
//  Copyright © 2018 JasonOnken. All rights reserved.
//

import UIKit

class AddItemViewController: UIViewController {

    @IBOutlet weak var itemNameET: UITextField!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var fullLabel: UILabel!
    @IBOutlet weak var fullSwitch: UISwitch!
    @IBOutlet weak var notesET: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func saveClicked(_ sender: Any) {
        
    }
    
    @IBAction func cancelClicked(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func stepperClicked(_ sender: Any) {
        
    }
    
    @IBAction func fullSwitchToggled(_ sender: Any) {
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
